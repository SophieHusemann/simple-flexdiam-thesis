"""

This is the documentation of the simple flexdiam addon package Thesis.
It contains ofowm run and/or arch configs, simple flexdiam issues, simple flexdiam observers, simple flexdiam tiers, simple flexdiam xmits, Rasa NLU trainingdata,  

# About the package

# Background / Literature

# Usage / Examples

## Installation
If you work on the package clone the repository and install it with `pip install -e .`:
```bash
git clone https://gitlab.ub.uni-bielefeld.de/SophieHusemann  ;  scs/aaambos  ;  scs/simple_flexdiam  ;  .../simple_flexdiam_pkg_thesis.git
cd simple_flexdiam_pkg_thesis
pip install -e .
```

If you just want to use the defined issues, etc. it might be sufficient just to install it to your site-packages.
```bash
pip install thesis @ git+https://gitlab.ub.uni-bielefeld.de/SophieHusemann  ;  scs/aaambos  ;  scs/simple_flexdiam  ;  .../simple_flexdiam_pkg_thesis@main
```

## RunningYou can run the here defined `arch_config` and `run_config` with:
```
conda activate aaambos
cd simple_flexdiam_pkg_thesis/thesis/configs
aaambos run --run_config run_config.yml --arch_config arch_config_agent_a.yml
```


You can add the here defined issues, etc. in your `arch_config` anywhere else.



# Citation


"""
import os
training_data_dir = os.path.dirname(os.path.realpath(__file__)) + "/training_data"