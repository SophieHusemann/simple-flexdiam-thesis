import uuid
from datetime import timedelta
from typing import Type, Any

from simple_flexdiam.modules.core.definitions import SpeechGenerationControl, Controls
from simple_flexdiam.modules.issue_flow_handler.issue import Issue, IssueOutput, IssueState
from simple_flexdiam.modules.issue_flow_handler.issue_flow_handler import Context
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig
from simple_flexdiam.modules.std.observers.nlu_entry_point_observer import NLU_PARSE_ENTRY_POINT
from simple_flexdiam.modules.std.xmits.speech_generation_xmit import SpeechGenerationXMit

''' if you want to require further observers via the issue, you can implement / overwrite the `further_observer_requirements` method
```
    @classmethod
    def further_observer_requirements(cls, issue_fh_config: IssueFHConfig) -> list[ObserverReq]:
        """Overwrite this function if the issue needs additional observers"""
        return []
```
You can specify additional aaambos architecture features in an issue similar to modules via the provides_features and requires_features methods:
```
    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        """Overwrite if necessary"""
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        """Overwrite if necessary"""
        return {}
```
You cannot add attributes to an issue class! Use the locals attribute instead:
`self.my_var = 123` does NOT work
`self.locals.my_var = 123` does not
'''

class IsEvidenceIssue(Issue):
    """Handles NLU entry points "", "" by ... under the condition ... """
    def initialize(self):
        # Setup part. Init of locals, etc. Here you CANNOT add xmits (e.g., speech output) or child issues.
        # Use the handle_prompt_request method instead. By checking for the first call. (create var in locals and update accordingly)
        pass

    @classmethod
    def get_init_shadow_children(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue] | tuple[Type[Issue], dict]]:
        # all issue classes from which the flow handler should create shadow children, when this issue is created.
        return []

    @classmethod
    def potential_children_classes(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue]]:
        # all classes that the issue could add as direct children. Including shadow issues.
        return []

    @classmethod
    def entry_points_of_interest(cls) -> list[str]:
        # add other entry points here if this issue needs to react/handle them.
        return [NLU_PARSE_ENTRY_POINT]

    @classmethod
    def potential_xmit_classes(cls) -> list[Type[XMit]]:
        # add other xmits here if this issue adds them to the output
        return [SpeechGenerationXMit]

    def handle_child_closed(self, child: Issue, context: Context, output: IssueOutput):
        # is called when a child issue reached an end state
        pass

    def can_handle_entry_point(self, entry_point: str, ep_data: Any, context) -> bool:
        # define when the issue can handle an entry point (e.g., by adding intentions, as strings, to the list)
        return entry_point == NLU_PARSE_ENTRY_POINT and ep_data.intention in ["is_evidence"]

    def handle_entry_point(self, entry_point: str, ep_data: Any, context: Context, output: IssueOutput):
        if self.can_handle_entry_point(entry_point, ep_data, context):  # should always be True

            # check if context has handled_topics attribute
            if not hasattr(context, "evidence_found"):
                context.evidence_found = set()

            # answer according to entity
            if ep_data.entities[0].value == "haare" or ep_data.entities[0].value == "haarbüschel":
                text = "<animations/Stand/Gestures/Yes_1> Ja, das ist ein Beweisgegenstand."
                self.say(text, floor_yield=5.0, output=output)
                context.evidence_found.add("hair")
            elif (ep_data.entities[0].value == "waffe" or ep_data.entities[0].value == "mordwaffe"
                  or ep_data.entities[0].value == "brieföffner" or ep_data.entities[0].value == "messer" ):
                text = "<animations/Stand/Gestures/Yes_1> Ja, das ist ein Beweisstück."
                self.say(text, floor_yield=5.0, output=output)
                context.evidence_found.add("weapon")
            elif (ep_data.entities[0].value == "schlüssel" or ep_data.entities[0].value == "schlüsselbund" or
                    ep_data.entities[0].value == "transponder" or ep_data.entities[0].value == "hinweis"):
                text = "<animations/Stand/Gestures/Yes_1> Ja, dieser Gegenstand zählt als Beweisstück."
                self.say(text, floor_yield=5.0, output=output)
                context.evidence_found.add("key")
            elif (ep_data.entities[0].value == "notiz" or ep_data.entities[0].value == "notizzettel" or
                    ep_data.entities[0].value == "name" or ep_data.entities[0].value == "name vom mörder" or
                    ep_data.entities[0].value == "fingerabdruck"):
                text = "<animations/Stand/Gestures/Yes_1> Ja, das ist ein Beweis für den Bericht."
                self.say(text, floor_yield=5.0, output=output)
                context.evidence_found.add("note")
            elif ep_data.entities[0].value == "leiche":
                text = "Die Leiche wurde schon zur Untersuchung abtransportiert."
                self.say(text, floor_yield=5.0, output=output)
            elif ep_data.entities[0].value == "fußspuren" or ep_data.entities[0].value == "fußabdrücke":
                text = "Von den Fußspuren wurden bereits Fotos gemacht und ins Labor geschickt."
                self.say(text, floor_yield=5.0, output=output)
            else:
                text = "<animations/Stand/Gestures/No_8> Das ist kein Beweisstück."
                self.say(text, floor_yield=5.0, output=output)

            context.repeat = text

            # set the state of the issue to an end state (FULFILLED, FAILED)
            self.set_end_state(IssueState.FULFILLED)


    def handle_prompt_request(self, context: Context, output: IssueOutput):
        # similar to the handle_entry_point method. Here you can add also issue execution logic.
        # This method is called on one active issue -> The flow handler request a prompt,
        # e.g., the agent could generate a speech output (floor is free)
        pass

    @staticmethod
    def say(text: str, floor_yield: float | int, output: IssueOutput) -> None:
        """Utility issue to add a speech output

        :param text: the utterance to generate
        :param floor_yield: the time to "yield"/block the floor afterward to allow the user to answer
        :param output: the issue output of the current execution (it is used to add the xmit)
        :return: None
        """

        speech_gen = SpeechGenerationControl(
            control=Controls.START,
            id=uuid.uuid4(),
            text=text,
        )
        xmit = SpeechGenerationXMit(speech_generation=speech_gen, floor_yield=timedelta(seconds=floor_yield))
        # similarly you can add other types of XMITs (== issue outputs)
        output.add_xmit(xmit)
