from datetime import timedelta
from typing import Type

from attrs import define, field

from simple_flexdiam.modules.core.observer import Observer
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit


@define(kw_only=True)
class ExampleXMit(XMit):
    """Xmit that ..."""
    # add fields/attributes here (attrs). For example, the data that is sent via the communications service.

    @classmethod
    def get_observer_class(cls) -> Type[Observer]:
        return ExampleObserver
