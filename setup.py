#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('CHANGELOG.md') as history_file:
    history = history_file.read()

requirements = [
    "simple_flexdiam @ git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_simple_flexdiam@main",
]

test_requirements = ['pytest>=3', ]

setup(
    author="Sophie Husemann",
    author_email='shusemannn@techfak.uni-bielefeld.de',
    python_requires='>=3.10',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: German',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.10',
    ],
    description="_description_",
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords=['aaambos', 'thesis'],
    name='thesis',
    packages=find_packages(include=['thesis', 'thesis.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.ub.uni-bielefeld.de/SophieHusemann  ;  scs/aaambos  ;  scs/simple_flexdiam  ;  .../simple_flexdiam_pkg_thesis',
    version='0.1.0',
    zip_safe=False,
)