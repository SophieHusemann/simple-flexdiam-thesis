import uuid
from datetime import timedelta
import random
from typing import Type, Any

from simple_flexdiam.modules.core.definitions import SpeechGenerationControl, Controls
from simple_flexdiam.modules.issue_flow_handler.issue import Issue, IssueOutput, IssueState
from simple_flexdiam.modules.issue_flow_handler.issue_flow_handler import Context
from simple_flexdiam.modules.issue_flow_handler.xmit import XMit
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig
from simple_flexdiam.modules.std.observers.nlu_entry_point_observer import NLU_PARSE_ENTRY_POINT
from simple_flexdiam.modules.std.xmits.speech_generation_xmit import SpeechGenerationXMit

''' if you want to require further observers via the issue, you can implement / overwrite the `further_observer_requirements` method
```
    @classmethod
    def further_observer_requirements(cls, issue_fh_config: IssueFHConfig) -> list[ObserverReq]:
        """Overwrite this function if the issue needs additional observers"""
        return []
```
You can specify additional aaambos architecture features in an issue similar to modules via the provides_features and requires_features methods:
```
    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        """Overwrite if necessary"""
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        """Overwrite if necessary"""
        return {}
```
You cannot add attributes to an issue class! Use the locals attribute instead:
`self.my_var = 123` does NOT work
`self.locals.my_var = 123` does not
'''

class EvidenceIssue(Issue):
    """Handles NLU entry points "", "" by ... under the condition ... """
    def initialize(self):
        # Setup part. Init of locals, etc. Here you CANNOT add xmits (e.g., speech output) or child issues.
        # Use the handle_prompt_request method instead. By checking for the first call. (create var in locals and update accordingly)
        pass

    @classmethod
    def get_init_shadow_children(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue] | tuple[Type[Issue], dict]]:
        # all issue classes from which the flow handler should create shadow children, when this issue is created.
        return []

    @classmethod
    def potential_children_classes(cls, config: SimpleFlexdiamConfig) -> list[Type[Issue]]:
        # all classes that the issue could add as direct children. Including shadow issues.
        return []

    @classmethod
    def entry_points_of_interest(cls) -> list[str]:
        # add other entry points here if this issue needs to react/handle them.
        return [NLU_PARSE_ENTRY_POINT]

    @classmethod
    def potential_xmit_classes(cls) -> list[Type[XMit]]:
        # add other xmits here if this issue adds them to the output
        return [SpeechGenerationXMit]

    def handle_child_closed(self, child: Issue, context: Context, output: IssueOutput):
        # is called when a child issue reached an end state
        pass

    def can_handle_entry_point(self, entry_point: str, ep_data: Any, context) -> bool:
        # define when the issue can handle an entry point (e.g., by adding intentions, as strings, to the list)
        return entry_point == NLU_PARSE_ENTRY_POINT and ep_data.intention in ["evidence"]

    def handle_entry_point(self, entry_point: str, ep_data: Any, context: Context, output: IssueOutput):
        if self.can_handle_entry_point(entry_point, ep_data, context):  # should always be True

            # check if context has handled_topics attribute
            if not hasattr(context, "handled_topics"):
                context.handled_topics = set()

            # check if context has handled_topics attribute
            if not hasattr(context, "evidence_found"):
                context.evidence_found = set()

            # check if context has last_answer attribute
            if not hasattr(context, "last_answer"):
                context.last_answer = ""

            topics_tried = 0
            topic_used = False
            text = ""

            # choose a random clue that was not mentioned yet
            while topic_used == False and topics_tried < 5:
                random_number = random.randint(1, 4)
                if random_number == 1:
                    topics_tried += 1
                    if "hair" not in context.handled_topics and "hair" not in context.evidence_found:
                        # "hair" is only in evidence_found, when the user asked if it was evidence
                        text = "<animations/Stand/Gestures/Look_1> Ich habe ein Haarbüschel auf dem Fluchtweg des Mörders zwischen seinen Fußspuren gesehen. Das ist ein Beweis."
                        self.say(text, floor_yield=5.0, output=output)
                        context.handled_topics.add("hair")
                        context.last_answer = "Das Haarbüschel auf dem Fluchtweg ist ein Beweis."
                        topic_used = True
                elif random_number == 2:
                    topics_tried += 1
                    if "weapon" not in context.handled_topics:
                        if "weapon" in context.evidence_found:
                            text = "Die gefundene Mordwaffe ist ein Beweisgegenstand."
                        else:
                            text = "<animations/Stand/Gestures/Thinking_8> Wenn wir die Mordwaffe identifiziert haben, können wir diese als Beweisgegenstand nehmen."
                        self.say(text, floor_yield=5.0, output=output)
                        context.handled_topics.add("weapon")
                        context.last_answer = "Die Mordwaffe ist ein Beweis."
                        topic_used = True
                elif random_number == 3:
                    topics_tried += 1
                    if "key" not in context.handled_topics and "key" not in context.evidence_found:
                        # "key" is only in evidence_found, when the user asked if it was evidence
                        text = "<animations/Stand/Gestures/Look_2> Mit meinem Sensor nehme ich die Signale eines unbekannten Transponders unter dem Schreibtisch war. Guck nach, ob der Mörder dort etwas verloren hat."
                        self.say(text, floor_yield=5.0, output=output)
                        context.handled_topics.add("key")
                        context.last_answer = "Der Transponder am Schlüsselbund ist ein Beweis."
                        topic_used = True
                elif random_number == 4:
                    topics_tried += 1
                    if "note" not in context.handled_topics:
                        if "note" in context.evidence_found:
                            text = "Die Notiz vom Mörder mit dem Fingerabdruck ist ein Beweisstück"
                        else:
                            text = "<animations/Stand/Gestures/Thinking_8> Der Mörder hat vielleicht eine Notiz hinterlassen, diese gilt bestimmt als Beweisstück"
                        self.say(text, floor_yield=5.0, output=output)
                        context.handled_topics.add("note")
                        context.last_answer = "Die Notiz vom Mörder ist ein Beweis."
                        topic_used = True

            # all clues were mentioned
            if topics_tried > 3:
                text = "<animations/Stand/Gestures/Look_2> Ich finde keine Beweisgegenstände mehr, aber wir haben auch schon genug."
                self.say(text, floor_yield=5.0, output=output)
                context.last_answer = "Wir haben genug Beweisgegenstände gesammelt."

            context.repeat = text

            # set the state of the issue to an end state (FULFILLED, FAILED)
            self.set_end_state(IssueState.FULFILLED)

    def handle_prompt_request(self, context: Context, output: IssueOutput):
        # similar to the handle_entry_point method. Here you can add also issue execution logic.
        # This method is called on one active issue -> The flow handler request a prompt,
        # e.g., the agent could generate a speech output (floor is free)
        pass

    @staticmethod
    def say(text: str, floor_yield: float | int, output: IssueOutput) -> None:
        """Utility issue to add a speech output

        :param text: the utterance to generate
        :param floor_yield: the time to "yield"/block the floor afterward to allow the user to answer
        :param output: the issue output of the current execution (it is used to add the xmit)
        :return: None
        """

        speech_gen = SpeechGenerationControl(
            control=Controls.START,
            id=uuid.uuid4(),
            text=text,
        )
        xmit = SpeechGenerationXMit(speech_generation=speech_gen, floor_yield=timedelta(seconds=floor_yield))
        # similarly you can add other types of XMITs (== issue outputs)
        output.add_xmit(xmit)
