# Thesis


This is a [Simple Flexidam](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/pkgs/dialog/) addon package. You can find more information about what Simple Flexdiam and AAAMBOS are [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/pkgs/dialog/simple_flexdiam/) and [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/what_is_aaambos) and how to install it [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/).

_Sophie's Master thesis. <br>
This package implements pepper robots for different escape rooms. There are agent A, B, and C._

## Install
If you work on the package clone the repository and install it with `pip install -e .`:
```bash
git clone https://gitlab.ub.uni-bielefeld.de/SophieHusemann/simple-flexdiam-thesis.git
cd simple_flexdiam_thesis
pip install -e .
```

If you just want to use the defined issues, etc. it might be sufficient just to install it to your site-packages.
```bash
pip install thesis @ git+https://gitlab.ub.uni-bielefeld.de/SophieHusemann/simple-flexdiam-thesis
```

## Running
To run this package, the [Rasa NLU](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_rasa_nlu) and [Vosk ASR](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_vosk_asr) packages are needed.

### Without a robot

The [Basis TTS](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_basic_tts) package is needed for the speech output.

The animations for the robot are annotated in the speech output, so the animation paths will be said and not played.

To run this programme without a robot, you can run the here defined `arch_config`'s and `run_config` with:
```
conda activate aaambos
cd simple_flexdiam_thesis/thesis/configs
aaambos run --run_config run_config.yml --arch_config arch_config_agent_a.yml
```

The ending of the `arch_config` depends on the agent. For agent B use `arch_config_agent_b.yml`, for agent C use `arch_config_agent_c.yml`

### With a robot

The [Naoqi Robot Embodiment](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_naoqi_robot_embodiment/-/tree/sophie_ma?ref_type=heads) package on branch `sophie_ma` is needed for the connection with the robot.

To run this programme with a pepper robot, you can run the here defined `arch_config_agent_x_pepper`'s and `run_config_pepper` with:
```
conda activate aaambos
cd simple_flexdiam_thesis/thesis/configs
aaambos run --run_config run_config_pepper.yml --arch_config arch_config_agent_a_pepper.yml
```

The letter of the `arch_config_agent_x_pepper` depends on the agent. For agent B use `arch_config_agent_b_pepper.yml`, for agent C use `arch_config_agent_c_pepper.yml`


