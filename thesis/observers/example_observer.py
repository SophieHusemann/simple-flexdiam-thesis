from aaambos.core.module.feature import Feature, FeatureNecessity

from simple_flexdiam.modules.core.features import TierReq, ObserverReq
from simple_flexdiam.modules.core.observer import Observer
from simple_flexdiam.modules.core.tier import ObservationType, Tier, IntervalEvent, Event, PointEvent
from simple_flexdiam.modules.simple_flexdiam_module import SimpleFlexdiamConfig


class ExampleObserver(Observer):
    last_agent_words = None

    @classmethod
    def provides_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def requires_features(cls, config: SimpleFlexdiamConfig, observer_config: dict = ...) -> dict[
        Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def get_tiers(cls, config: SimpleFlexdiamConfig, observer_config: dict) -> dict[TierReq, ObservationType | list[ObservationType]]:
        # define here the tiers the observer wants to read, write, and observe (observe -> handle_new_event is called)
        # the tier already handles the com (In/Out) features
        return {}

    async def initialize(self):
        await super().initialize()

    async def handle_new_event(self, tier: Tier, event: Event):
        ...
        """you can add new events to a tier (add write observation type to the get_tiers method):
        ```
        my_event = IntervalEvent(start_point=datetime.now(), data={}, short_view="shortInfoHereForGUI")  # end_point=datetime.now() + timedelta(seconds=10)
        my_point_event = PointEvent(time_point=datetime.now(), data={}, short_view="shortInfoHereForGUI")
        await self.timeboard.add_event(ExampleTier.tier_name, my_event)
        await self.timeboard.add_event(OtherExampleTier.tier_name, my_point_event)
        ```
        you can update events:
        ```
        my_event.end_point = datetime.now() + timedelta(seconds=10)
        await self.timeboard.update_event(ExampleTier.tier_name, my_event)
        ```
        you can call enter entry points to the flow handler by calling:
        ```
        await self.flow_handler.process_entry_point(NLU_PARSE_ENTRY_POINT, data, self.timeboard)
        ```
        Change the entry point name and data accordingly.
        """


# specify this in the `further_observer_requirements`-method of an issue that you use. (Or the flow handler directly)
#exampl_observer_req = ObserverReq(oberserver_class=ExampleObserver, oberserver_config={})
